/**
 * Copyright (c) 2013 IMPACT Lab of Arizona State University.  
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the Arizona State University nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package com.majora.minecraft.effects;

/**
 * Particle Types are from https://gist.github.com/thinkofdeath/5110835
 */
public final class ParticleType
{
	public static String HUGE_EXPLOSION = "hugeexplosion";
	public static String LARGE_EXPLODE = "largeexplode";
	public static String FIREWORKS_SPARK = "fireworksSpark";
	public static String BUBBLE = "bubble";
	public static String SUSPENDED = "suspended";
	public static String DEPTH_SUSPEND = "depthsuspend";
	public static String TOWN_AURA = "townaura";
	public static String CRIT = "crit";
	public static String MAGIC_CRIT = "magicCrit";
	public static String SMOKE = "smoke";
	public static String MOB_SPELL = "mobSpell";
	public static String MOB_SPELL_AMBIENT = "mobSpellAmbient";
	public static String SPELL = "spell";
	public static String INSTANT_SPELL = "instantSpell";
	public static String WITCH_MAGIC = "witchMagic";
	public static String NOTE = "note";
	public static String PORTAL = "portal";
	public static String ENCHANTMENT_TABLE = "enchantmenttable";
	public static String EXPLODE = "explode";
	public static String FLAME = "flame";
	public static String LAVA = "lava";
	public static String FOOT_STEP = "footstep";
	public static String SPLASH = "splash";
	public static String LARGE_SMOKE = "largesmoke";
	public static String CLOUD = "cloud";
	public static String RED_DUST = "reddust";
	public static String SNOWBALL_POOF = "snowballpoof";
	public static String DRIP_WATER = "dripWater";
	public static String DRIP_LAVA = "dripLava";
	public static String SNOW_SHOVEL = "snowshovel";
	public static String SLIME = "slime";
	public static String HEART = "heart";
	public static String ANGRY_VILLAGER = "angryVillager";
	public static String HAPPY_VILLAGER = "happyVillager";
	//public static String ICON_CRACK = "iconcrack_*"; //iconcrack_*
	//public static String TILE_CRACK = "tilecrack_*_*"; //tilecrack_*_*
}
