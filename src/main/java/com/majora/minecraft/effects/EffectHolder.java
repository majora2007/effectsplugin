/**
 * Copyright (c) 2013 IMPACT Lab of Arizona State University.  
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the Arizona State University nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package com.majora.minecraft.effects;

import java.util.HashSet;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class EffectHolder extends BukkitRunnable {

	private HashSet<Effect> effects = new HashSet<Effect>();
	private BukkitTask task = null;
	protected EffectType effectType;
	protected EffectDetails details;

	public World world;
	public int locX;
	public int locY;
	public int locZ;

	public EffectHolder(EffectType effectType) {
		this.effectType = effectType;
		this.details = new EffectDetails(effectType);
	}

	public void setEffects(HashSet<Effect> effects) {
		this.effects = effects;
	}

	public void updateLocation(Location l) {
		this.locX = l.getBlockX();
		this.locY = l.getBlockY();
		this.locZ = l.getBlockZ();
	}

	public void updateLocation(int x, int y, int z) {
		this.locX = x;
		this.locY = y;
		this.locZ = z;
	}

	public EffectDetails getDetails() {
		return this.details;
	}

	public void start() {
		this.task = this.runTaskTimer(EffectsPlugin.getInstance(), 0L, 20); // max tick
	}

	public void run() {
		for (Effect e : effects) {
			if (20 % 20 == 0) {
				//e.play();
			}
		}
	}

	public void stop() {
		this.task.cancel();
	}

	public enum EffectType {

		LOCATION, PLAYER, MOB;
	}
}
