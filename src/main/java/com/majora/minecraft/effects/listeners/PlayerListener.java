/**
 * Copyright (c) 2013 IMPACT Lab of Arizona State University.  
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the Arizona State University nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package com.majora.minecraft.effects.listeners;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitTask;

import com.majora.minecraft.effects.EffectsPlugin;
import com.majora.minecraft.effects.PacketFactory;
import com.majora.minecraft.effects.ParticleType;
import com.majora.minecraft.effects.RepeatEffectTask;
import com.majora.minecraft.effects.utils.ReflectionUtil;

public class PlayerListener implements Listener
{
	private EffectsPlugin plugin = null;
	private BukkitTask effectTask;
	
	private List<BukkitTask> effectTasks = new ArrayList<BukkitTask>();
	
	public PlayerListener(EffectsPlugin plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event)
	{
		String[] tokens = event.getMessage().split(" ");
		EffectsPlugin.log( "Tokens length: " + tokens.length );
		
		if (tokens.length < 1 || !tokens[0].equalsIgnoreCase( "/effects" )) return;
		
		if (tokens.length <= 3)
		{
			if (tokens[1].equalsIgnoreCase( "window" ) && tokens[2].equalsIgnoreCase( "open" ))
			{
				Object windowPacket = PacketFactory.createOpenWindowPacket(123, 0, "Effect Inventory", 4, true, 0);
				sendPacket(event.getPlayer(), windowPacket);
			} else if (tokens[1].equalsIgnoreCase( "play" ))
			{
				String effect = tokens[2];
				Block block = event.getPlayer().getTargetBlock( null, 5 );
				
				Object particlePacket = PacketFactory.createParticlePacket( effect, block, 0, 0.5f, 0.5f, 1.0f, 15 );
				sendPacket(event.getPlayer(), particlePacket);
				event.getPlayer().sendMessage( "Particle sent" );
			}
		}
		
		if (tokens.length == 8) // xps play effectName xoffset, yoffset, zoffset, speed, particles
		{
			if (tokens[1].equalsIgnoreCase( "play" ))
			{
				String effect = tokens[2];
				Block block = event.getPlayer().getTargetBlock( null, 5 );
				
				Object particlePacket = PacketFactory.createParticlePacket( effect, block, Float.parseFloat(tokens[3]), Float.parseFloat(tokens[4]), Float.parseFloat(tokens[5]), Float.parseFloat(tokens[6]), Integer.parseInt(tokens[7]) );
				//sendPacket(event.getPlayer(), particlePacket);
				
				effectTasks.add(new RepeatEffectTask( this.plugin, particlePacket, event.getPlayer().getLocation() ).runTaskTimer( this.plugin, 20, 20 ));
				
			}
			
		}
	}
	
	@EventHandler
	public void onPlayerClick(final PlayerInteractEvent event)
	{
		if (event.isCancelled()) return;
		
		if(!event.hasBlock()) return;
		
		// If player left clicks on a block, play an effect on the block
		final Player player = event.getPlayer();
		final Block clickedBlock = event.getClickedBlock();
		
		if (event.getAction() == Action.LEFT_CLICK_BLOCK)
		{
			player.playSound( clickedBlock.getLocation(), Sound.ANVIL_LAND, 1.0f, 4.0f );
			
		} else if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
		{
			Object particlePacket = PacketFactory.createParticlePacket( ParticleType.ENCHANTMENT_TABLE, clickedBlock, 0, 0.5f, 0.5f, 1.0f, 15 );
			sendPacket(event.getPlayer(), particlePacket);

		}
	}
	
	private void sendPacket(Player player, Object packet)
	{
		try
		{
			ReflectionUtil.sendPacket( player.getLocation(), packet );
		} catch ( SecurityException | NoSuchMethodException
				| IllegalArgumentException | IllegalAccessException
				| InvocationTargetException | NoSuchFieldException ex )
		{
			ex.printStackTrace();
		}
	}

	public List<BukkitTask> getTasks() {
		return effectTasks;
	}
}
